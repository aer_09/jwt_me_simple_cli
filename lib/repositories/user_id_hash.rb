# frozen_string_literal: true
require 'singleton'
require_relative '../validations/common'
require_relative '../utilities/parser'
require_relative '../utilities/messages'

module Repositories
  class UserIdHash
    include Singleton

    def create(key_number_digit=nil)
      set_validator

      @hash = {}

      get_key_and_value

      @hash
    end

    def get_key_and_value(key_number_digit=nil)
      key_number = 1

      key = parser.parse_key(key_number)

      key_valid = @validator.valid?(
        key,
        :user_id_key?,
        Utilities::MESSAGES[:add_user_id_to_payload][:invalid_key],
        :get_key_and_value
      )

      if key_valid
        get_value(key)
      end
    end

    def get_value(key)
      value = parser.parse_value(Utilities::MESSAGES[:add_user_id_to_payload][:value])

      user_id_valid = @validator.valid?(
        value,
        :present?,
        Utilities::MESSAGES[:invalid_value],
        :get_value,
        key
      )

      @hash[key] = value if user_id_valid
    end

    def parser
      Utilities::Parser.instance
    end

    def set_validator
      @validator = Validations::Common.new(Repositories::UserIdHash)
    end
  end
end
