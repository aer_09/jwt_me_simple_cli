# frozen_string_literal: true
require 'singleton'
require_relative '../validations/common'
require_relative '../utilities/parser'
require_relative '../utilities/messages'

module Repositories
  class Payload
    include Singleton

    def create
      @payload = {}
    end

    def result
      @payload
    end

    def update(hash)
      key, value = hash.to_a.flatten
      @payload[key] = value
    end
  end
end
