# frozen_string_literal: true
require 'singleton'
require_relative '../validations/common'
require_relative '../utilities/parser'
require_relative '../utilities/messages'

module Repositories
  class AdditionalHashes
    include Singleton

    def create
      set_validator
      @key_number = 2
      @array_of_hashes = []

      add_additional_hashes_to_payload

      @array_of_hashes
    end

    def add_additional_hashes_to_payload(key_number_digit=nil)
      puts Utilities::MESSAGES[:add_additional_hashes_to_payload][:question]
      answer = gets.chomp

      answer_valid = @validator.valid?(
        answer,
        :answer_valid?,
        Utilities::MESSAGES[:add_additional_hashes_to_payload][:invalid_answer],
        :add_additional_hashes_to_payload
      )

      if answer_valid
        get_hash_and_value if @validator.answer_yes?(answer)
      end
    end

    def get_hash_and_value(key_number_digit=nil)
      key_number = key_number_digit || @key_number += 1

      key = parser.parse_key(key_number)
      key_valid = @validator.valid?(
        key,
        :present?,
        Utilities::MESSAGES[:add_additional_hash_to_payload][:invalid_key],
        :get_hash_and_value,
        key_number
      )

      if key_valid
        get_value(key)

        add_additional_hashes_to_payload
      end
    end

    def get_value(key)
      value = parser.parse_value("Enter #{key} value")

      additional_hash_value_valid = @validator.valid?(
        value,
        :present?,
        Utilities::MESSAGES[:invalid_value],
        :get_value,
        key
      )

      @array_of_hashes << { key => value } if additional_hash_value_valid
    end

    def parser
      Utilities::Parser.instance
    end

    def set_validator
      @validator = Validations::Common.new(Repositories::AdditionalHashes)
    end
  end
end
