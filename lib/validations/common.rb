# frozen_string_literal: true
require 'singleton'
require_relative '../repositories/payload'

module Validations
  class Common
    def initialize(repo_name)
      @repo_name = repo_name
    end

    EMAIL_REGEXP = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.freeze
    REQUIRED_KEYS = %w(user_id email).freeze
    VALID_ANSWERS = %w(yes no).freeze

    def valid_email?(email)
      (email =~ EMAIL_REGEXP) != nil
    end

    def answer_valid?(answer)
      VALID_ANSWERS.include?(answer)
    end

    def answer_yes?(answer)
      answer == VALID_ANSWERS.first
    end

    def user_id_key?(key)
      REQUIRED_KEYS.first == key
    end

    def email_key?(key)
      REQUIRED_KEYS.last == key
    end

    def present?(value)
      !value.empty?
    end

    def valid?(key, validation_method, validation_message, recursive_method, recursive_method_value=nil)
      if !self.public_send(validation_method, key)
        puts validation_message

        @repo_name.instance.public_send(recursive_method, recursive_method_value)

        false
      else
        true
      end
    end
  end
end
