# frozen_string_literal: true

module Utilities
  MESSAGES = {
    welcome: 'Starting with JWT token generation.',
    bye: 'The JWT has been copied to your clipboard!',
    enter_key: 'Enter key ',
    invalid_value: 'NOTICE: Value can\'t be empty string',

    add_user_id_to_payload: {
      value: 'Enter user_id value',
      invalid_key: 'NOTICE: Please add user_id key'
    },
    add_email_to_payload: {
      value: 'Enter email value',
      invalid_key: 'NOTICE: Please add email key',
      invalid_email: 'NOTICE: Invalid email'
    },
    add_additional_hash_to_payload: {
      invalid_key: 'NOTICE: Please add non empty key'
    },
    add_additional_hashes_to_payload: {
      question: 'Any additional inputs? (yes/no)',
      invalid_answer: 'NOTICE: Please answer yes/no'
    }
  }.freeze
end
