# frozen_string_literal: true
require 'singleton'

module Utilities
  class Parser
    include Singleton

    STDOUT.sync = true
    
    def parse_key(key_number)
      puts MESSAGES[:enter_key] + key_number.to_s
      gets.chomp
    end

    def parse_value(message)
      puts message
      gets.chomp
    end
  end
end
