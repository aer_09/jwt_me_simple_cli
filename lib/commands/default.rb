require 'jwt'
require_relative '../repositories/payload'
require_relative '../repositories/email_hash'
require_relative '../repositories/user_id_hash'
require_relative '../repositories/additional_hashes'
require_relative '../utilities/messages'

module Commands
  module Default
    def run
      @payload_repository = Repositories::Payload.instance
      @payload_repository.create

      puts Utilities::MESSAGES[:welcome]

      user_id_hash = Repositories::UserIdHash.instance.create
      email_hash = Repositories::EmailHash.instance.create
      additional_hashes_array = Repositories::AdditionalHashes.instance.create

      @payload_repository.update(user_id_hash)
      @payload_repository.update(email_hash)

      additional_hashes_array.each do |hash|
        @payload_repository.update(hash)
      end

      token = JWT.encode(@payload_repository.result, 'my$ecretK3y', 'HS256')
      IO.popen('pbcopy', 'w') { |pipe| pipe << token }

      puts Utilities::MESSAGES[:bye]

      # DEBUG payload
      # puts @payload_repository.result
    end
  end
end
